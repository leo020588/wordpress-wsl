#!/bin/bash

sudo /etc/init.d/php8.3-fpm start
sudo /etc/init.d/php8.2-fpm start
sudo /etc/init.d/php8.1-fpm start
sudo /etc/init.d/mariadb start
sudo /etc/init.d/redis-server start
sudo /etc/init.d/openresty start
