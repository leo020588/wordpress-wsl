#!/bin/bash

sudo /etc/init.d/php8.3-fpm stop
sudo /etc/init.d/php8.2-fpm stop
sudo /etc/init.d/php8.1-fpm stop
sudo /etc/init.d/mariadb stop
sudo /etc/init.d/redis-server stop
sudo /etc/init.d/openresty stop
