#!/bin/bash

set -eux


# base packages
apt-get update
apt-get install -y --no-install-recommends openssl wget curl apt-transport-https ca-certificates lsb-release debian-archive-keyring gnupg2 htop unzip p7zip


# setup user
usermod --password $(echo web | openssl passwd -1 -stdin) web
usermod -aG sudo web
echo 'web ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/web


# copy folder repository to current system if needed
CURRDIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
CURRDIR=$(dirname $CURRDIR)
BASEDIR=/home/web/wordpress

if [ "$CURRDIR" != "$BASEDIR" ]; then
	cp -r $CURRDIR $BASEDIR
fi


# setup apt repositories
if ! grep -q " non-free" "/etc/apt/sources.list"; then
	sed -i 's/main/main non-free/g' /etc/apt/sources.list
fi

if ! grep -q " contrib" "/etc/apt/sources.list"; then
	sed -i 's/main/main contrib/g' /etc/apt/sources.list
fi

curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg
sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
wget -O- https://openresty.org/package/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/openresty.org.gpg > /dev/null
sh -c 'echo "deb [signed-by=/usr/share/keyrings/openresty.org.gpg] http://openresty.org/package/debian $(lsb_release -sc) openresty" > /etc/apt/sources.list.d/openresty.list'


# install required packages
apt-get update && apt-get upgrade -y && apt-get install -y \
 php8.1-bcmath php8.1-bz2 php8.1-cli php8.1-curl php8.1-fpm php8.1-gd php8.1-gmp php8.1-igbinary php8.1-imagick php8.1-intl php8.1-mbstring php8.1-mysql php8.1-opcache php8.1-readline php8.1-redis php8.1-soap php8.1-sqlite3 php8.1-xml php8.1-xsl php8.1-zip php8.1-ast \
 php8.2-bcmath php8.2-bz2 php8.2-cli php8.2-curl php8.2-fpm php8.2-gd php8.2-gmp php8.2-igbinary php8.2-imagick php8.2-intl php8.2-mbstring php8.2-mysql php8.2-opcache php8.2-readline php8.2-redis php8.2-soap php8.2-sqlite3 php8.2-xml php8.2-xsl php8.2-zip php8.2-ast \
 php8.3-bcmath php8.3-bz2 php8.3-cli php8.3-curl php8.3-fpm php8.3-gd php8.3-gmp php8.3-igbinary php8.3-imagick php8.3-intl php8.3-mbstring php8.3-mysql php8.3-opcache php8.3-readline php8.3-redis php8.3-soap php8.3-sqlite3 php8.3-xml php8.3-xsl php8.3-zip php8.3-ast \
 openresty \
 mariadb-server mariadb-client \
 redis-server \
 git git-flow


# nodejs
mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
apt-get update
apt-get install -y nodejs


# cwebp manual install
cd /root
wget https://storage.googleapis.com/downloads.webmproject.org/releases/webp/libwebp-1.4.0-linux-x86-64.tar.gz
tar -xzf libwebp-1.4.0-linux-x86-64.tar.gz
cp libwebp-1.4.0-linux-x86-64/bin/cwebp /usr/local/bin
chmod a+x /usr/local/bin/cwebp
rm -rf libwebp*


# openresty setup
mkdir -p /var/log/nginx/
ln -s -d /usr/local/openresty/nginx/conf /etc/nginx
cp $BASEDIR/config/nginx/nginx.conf /etc/nginx/nginx.conf
mkdir /etc/nginx/conf.d && chmod 777 /etc/nginx/conf.d


# php setup
for PHP_VERSION in 8.1 8.2 8.3; do
	for PHP_SAPI in cli fpm; do
		PHP_INI=/etc/php/${PHP_VERSION}/${PHP_SAPI}/php.ini

		sed -i 's|;realpath_cache_size = 4096k|realpath_cache_size = 5M|' $PHP_INI
		sed -i 's|expose_php = On|expose_php = Off|' $PHP_INI
		sed -i 's|max_execution_time = 30|max_execution_time = 60|' $PHP_INI
		sed -i 's|memory_limit = 128M|memory_limit = 1024M|' $PHP_INI
		sed -i 's|display_errors = Off|display_errors = On|' $PHP_INI
		sed -i 's|ignore_repeated_errors = Off|ignore_repeated_errors = On|' $PHP_INI
		sed -i 's|ignore_repeated_source = Off|ignore_repeated_source = On|' $PHP_INI
		sed -i 's|;html_errors = On|html_errors = Off|' $PHP_INI
		sed -i 's|post_max_size = 8M|post_max_size = 10M|' $PHP_INI
		sed -i 's|upload_max_filesize = 2M|upload_max_filesize = 10M|' $PHP_INI
		sed -i 's|;date.timezone =|date.timezone = America/Lima|' $PHP_INI
		sed -i 's|;mbstring.language = Japanese|mbstring.language = Neutral|' $PHP_INI
	done

	sed -i 's|www-data|web|g' /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf
done


# composer
curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
chmod a+x /usr/local/bin/composer


# wp-cli (locally patched)
cp $BASEDIR/wp-cli/wp-cli.phar /usr/local/bin/wp
chmod +x /usr/local/bin/wp


# localhost setup
mkdir -p /home/web/sites/localhost
cp $BASEDIR/config/nginx/default.conf /etc/nginx/conf.d/default.conf
cp $BASEDIR/resources/adminer.php /home/web/sites/localhost/adminer.php
cp $BASEDIR/resources/redis.php /home/web/sites/localhost/redis.php
echo '<?php phpinfo();' > /home/web/sites/localhost/index.php
chown -R web:web /home/web/sites


# customize: ls, npm, composer
cat <<EOT > /home/web/.myrc
# aliases
# -------------------------------------
alias ls='ls -Alhv --group-directories-first --color=auto'


# NPM
# -------------------------------------
NPM_PACKAGES="\$HOME/.npm-packages"
PATH="\$NPM_PACKAGES/bin:\$PATH"
NODE_PATH="\$NPM_PACKAGES/lib/node_modules:\$NODE_PATH"


# COMPOSER
# -------------------------------------
COMPOSER_HOME=/home/web/.config/composer
PATH=/home/web/.config/composer/vendor/bin:\$PATH
EOT

cat <<EOT >> /home/web/.bashrc

# .myrc
# -------------------------------------
source ~/.myrc
EOT

echo 'prefix=/home/web/.npm-packages' > /home/web/.npmrc


# reset owner
chown -R web:web /home/web


# start services
/etc/init.d/php8.3-fpm start
/etc/init.d/php8.2-fpm start
/etc/init.d/php8.1-fpm start
/etc/init.d/mariadb start
/etc/init.d/openresty start
/etc/init.d/redis-server start


# db setup
printf '%s\n' '' Y Y root root Y Y Y Y | sudo mariadb-secure-installation
