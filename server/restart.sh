#!/bin/bash

sudo /etc/init.d/php8.3-fpm restart
sudo /etc/init.d/php8.2-fpm restart
sudo /etc/init.d/php8.1-fpm restart
sudo /etc/init.d/mariadb restart
sudo /etc/init.d/redis-server restart
sudo /etc/init.d/openresty restart
