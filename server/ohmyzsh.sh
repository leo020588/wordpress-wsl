#!/bin/bash

set -eux

sudo apt-get install -y wget zsh git
sudo usermod -s /usr/bin/zsh web
wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O ohmyzsh-install.sh
CHSH=no RUNZSH=no KEEP_ZSHRC=no sh ohmyzsh-install.sh
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.powerlevel10k
echo 'source ~/.powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
echo 'source ~/.myrc' >>~/.zshrc
rm -f ohmyzsh-install.sh
