# Wordpress on WSL

## Required software
- Windows 10, Windows 11
- [Git](https://git-scm.com/downloads)
- [Terminal](https://www.microsoft.com/en-us/p/windows-terminal/9n0dx20hk701)
- [Visual Studio Code](https://code.visualstudio.com/)

## Server installation

### 1. Clone this repository
*PowerShell*:
```bash
git clone https://gitlab.com/leo020588/wordpress-wsl.git D:\zerver\system\other\wordpress
```

### 2. Install WSL Debian
*PowerShell*:
```bash
wsl --install -d Debian
```
- Username: `web`
- Password: `web`

### 3. Install server environment
*Debian*:
```bash
sudo bash /mnt/d/zerver/system/other/wordpress/server/install.sh
```

### 4. Oh My Zsh + powerlevel10k
*Debian*:
```bash
bash /mnt/d/zerver/system/other/wordpress/server/ohmyzsh.sh
```

### 5. Start/Stop the server
*Debian*:
```bash
~/wordpress/server/start.sh
~/wordpress/server/stop.sh
~/wordpress/server/restart.sh
```


## Wordpress installation

To create a `test.wp.net` site:

### 1. Install wordpress site
*Debian*:
```bash
~/wordpress/create-site.sh Test
```

### 2. Install certificate and update hosts
*Windows*:
- Install `C:\wordpress\config\certs\Localhost_Root_Certification_Authority.crt`
- Add entry `127.0.0.1 test.wp.net` to `C:\Windows\System32\drivers\etc\hosts`
- Navigate:
  * Wordpress https://test.wp.net:8888/ (admin/admin)
  * MariaDB https://localhost:8888/adminer.php (root/root)
  * Redis https://localhost:8888/redis.php


### 3. Open the project in vscode
*Debian*:
```bash
code ~/sites/test
```
