#!/bin/bash

randstr () {
	</dev/urandom tr -dc '1234567890abcdefghijklmnopqrstuvwxyz' | head -c$1; echo ""
}

WP_OS=$(uname -a)

if [[ $WP_OS == *"MINGW64"* ]]; then
	BASE_PATH=D:/zerver/sites/wordpress
	NGINXCONF_PATH=D:/zerver/system/nginx/conf/sites/wordpress
	WORDPRESS_UTILS=D:/zerver/system/other/wordpress
else
	BASE_PATH=/home/web/sites
	NGINXCONF_PATH=/etc/nginx/conf.d
	WORDPRESS_UTILS=/home/web/wordpress
fi

SERVER_PORT=443

WP_TITLE=$1
WP_ID=${WP_TITLE,,}
WP_HOST=${WP_ID}.wp.net
WP_URL=https://${WP_HOST}

WP_VERSION=latest
WP_PROJECT_DIR=$BASE_PATH/$WP_ID
WP_DIR=$WP_PROJECT_DIR/wordpress
WP_LOG_FILE=$WP_PROJECT_DIR/logs/debug.log
WP_NGINX_CACHE_PATH=$WP_DIR/wp-content/cache/nginx

WP_DB_NAME=wordpress_${WP_ID}
WP_DB_USER=root
WP_DB_PASSWORD=root
WP_DB_HOST=localhost
WP_DB_CHARSET=utf8mb4
WP_DB_COLLATE=utf8mb4_unicode_520_ci
WP_DB_PREFIX=wp$(randstr 3)_

WP_THEME_SLUG="${WP_ID,,}-theme"
WP_THEME_NAME="${WP_TITLE} Theme"
WP_THEME_NAMESPACE="${WP_TITLE}Theme"
WP_THEME_CONSTANT_PREFIX=${WP_TITLE^^}_THEME
WP_THEME_BASE_PATH=$WP_DIR/wp-content/themes
WP_THEME_PATH=$WP_THEME_BASE_PATH/$WP_THEME_SLUG

WP_THEME_REF=empty-theme
WP_THEME_REF_PATH=$WORDPRESS_UTILS/resources/$WP_THEME_REF


if [ -z "$WP_ID" ]; then
	exit
fi

if [ -d "$WP_DIR" ]; then
	rm -rf "${BASE_PATH}/${WP_ID}"
fi

mkdir -p $WP_DIR

cd $WP_DIR



# DB SETUP --------------------

mysql -u $WP_DB_USER -p$WP_DB_PASSWORD -e "DROP DATABASE IF EXISTS $WP_DB_NAME; CREATE DATABASE $WP_DB_NAME CHARACTER SET $WP_DB_CHARSET COLLATE $WP_DB_COLLATE;"


# WP INSTALL --------------------

wp core download --locale=en_US --version=$WP_VERSION --skip-content
wp config create --dbname=$WP_DB_NAME --dbuser=$WP_DB_USER --dbpass=$WP_DB_PASSWORD --dbcharset=$WP_DB_CHARSET --dbcollate=$WP_DB_COLLATE --dbprefix=$WP_DB_PREFIX
wp core install --url=${WP_URL} "--title=$WP_TITLE" --admin_user=admin --admin_password=admin --admin_email=admin@local.com --skip-email

mkdir -p $WP_PROJECT_DIR/logs
mkdir -p $WP_DIR/wp-content/plugins


# WP OPTIONS --------------------

mkdir -p $WP_NGINX_CACHE_PATH

echo '/%postname%/' | wp option update permalink_structure

wp option update start_of_week '0'
wp option update timezone_string 'America/Lima'


# WP THEME --------------------

mkdir -p $WP_THEME_BASE_PATH

cp -R $WP_THEME_REF_PATH $WP_THEME_BASE_PATH

mv $WP_THEME_BASE_PATH/$WP_THEME_REF $WP_THEME_PATH

find $WP_THEME_PATH -type f -print0 | xargs -0 sed -i "s/empty-theme/$WP_THEME_SLUG/g"
find $WP_THEME_PATH -type f -print0 | xargs -0 sed -i "s/Empty Theme/$WP_THEME_NAME/g"
find $WP_THEME_PATH -type f -print0 | xargs -0 sed -i "s/EmptyTheme/$WP_THEME_NAMESPACE/g"
find $WP_THEME_PATH -type f -print0 | xargs -0 sed -i "s/EMPTY_THEME/$WP_THEME_CONSTANT_PREFIX/g"

cd $WP_DIR

wp theme activate $WP_THEME_SLUG


# WP CONFIG --------------------

cp -f $WORDPRESS_UTILS/resources/wordpress.gitignore $WP_PROJECT_DIR/.gitignore
cp -f $WORDPRESS_UTILS/resources/wp-config.php $WP_DIR

sed -i "s|~DB_NAME~|$WP_DB_NAME|" $WP_DIR/wp-config.php
sed -i "s|~DB_USER~|$WP_DB_USER|" $WP_DIR/wp-config.php
sed -i "s|~DB_PASSWORD~|$WP_DB_PASSWORD|" $WP_DIR/wp-config.php
sed -i "s|~DB_HOST~|$WP_DB_HOST|" $WP_DIR/wp-config.php
sed -i "s|~DB_CHARSET~|$WP_DB_CHARSET|" $WP_DIR/wp-config.php
sed -i "s|~DB_COLLATE~|$WP_DB_COLLATE|" $WP_DIR/wp-config.php
sed -i "s|~DB_PREFIX~|$WP_DB_PREFIX|" $WP_DIR/wp-config.php
sed -i "s|~WP_DEBUG_LOG~|$WP_LOG_FILE|" $WP_DIR/wp-config.php
sed -i "s|~THEME_SLUG~|$WP_THEME_SLUG|" $WP_DIR/wp-config.php
sed -i "s|~AUTH_KEY~|$(randstr 64)|" $WP_DIR/wp-config.php
sed -i "s|~AUTH_SALT~|$(randstr 64)|" $WP_DIR/wp-config.php
sed -i "s|~SECURE_AUTH_KEY~|$(randstr 64)|" $WP_DIR/wp-config.php
sed -i "s|~SECURE_AUTH_SALT~|$(randstr 64)|" $WP_DIR/wp-config.php
sed -i "s|~LOGGED_IN_KEY~|$(randstr 64)|" $WP_DIR/wp-config.php
sed -i "s|~LOGGED_IN_SALT~|$(randstr 64)|" $WP_DIR/wp-config.php
sed -i "s|~NONCE_KEY~|$(randstr 64)|" $WP_DIR/wp-config.php
sed -i "s|~NONCE_SALT~|$(randstr 64)|" $WP_DIR/wp-config.php
sed -i "s|~WP_CACHE_KEY_SALT~|$WP_DB_PREFIX|" $WP_DIR/wp-config.php


# CLEAN FILES --------------------

FILES_TO_CLEAN=(
	".htaccess"
	"readme.html"
	"license.txt"
	"wp-config-sample.php"
)

for file in "${FILES_TO_CLEAN[@]}"; do
	if [ -f "$WP_DIR/$file" ]; then
		rm $WP_DIR/$file
	fi
done


# NGINX CONF --------------------


NGINXCONF_FILE=$WP_PROJECT_DIR/etc/nginx-server.conf
NGINXCONF_LINK=$NGINXCONF_PATH/$WP_HOST.conf

mkdir -p $WP_PROJECT_DIR/etc
rm $NGINXCONF_LINK

cp $WORDPRESS_UTILS/config/nginx/wp_server.conf $NGINXCONF_FILE

sed -i "s|{CACHE_DIR}|$WP_NGINX_CACHE_PATH|g" $NGINXCONF_FILE
sed -i "s|{HOST}|$WP_HOST|g" $NGINXCONF_FILE
sed -i "s|{PORT}|$SERVER_PORT|g" $NGINXCONF_FILE
sed -i "s|{BASE_DIR}|$WP_PROJECT_DIR|g" $NGINXCONF_FILE
sed -i "s|{CONF_DIR}|$WORDPRESS_UTILS/config|g" $NGINXCONF_FILE

ln -s $NGINXCONF_FILE $NGINXCONF_LINK


# SERVICES --------------------

if [[ ! $WP_OS == *"MINGW64"* ]]; then
	~/wordpress/server/restart.sh
fi
