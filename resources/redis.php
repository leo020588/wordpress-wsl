<?php

error_reporting(E_ALL & ~E_NOTICE);

if (!class_exists("Redis")) {
	die ("<b>Fatal error</b>: <b>php_redis</b> extension is not enabled.");
}

$redis = new Redis();
$addr  = '127.0.0.1';
$port  = 6379;

try {
	$redis->connect($addr, $port, 0.05);

	if (isset($_GET['pingRedis']) && !empty($_GET['pingRedis'])) {
		$infoRedis = $redis->info();
		header("X-pingRedis: Redis/{$infoRedis['redis_version']}");
		die("PONG");
	}

} catch (Exception $e) {
	die("Error: Unable to connect: ".$e->getMessage());
}

$db_num = (isset($_REQUEST['a']) && $_REQUEST['a'] == 'i') ? null : intval($_REQUEST['db'] ?? 0);

$script_name = $_SERVER['SCRIPT_NAME'] ?? "index.php";

?>
<html>
	<head>
		<title>Redis Cache Manager</title>
		<style type="text/css">
		html {
			background: #ddd;
		}
		body {
			font-family:  tahoma, arial, helvetica, lucida sans, sans-serif;
			margin: 20px;
			padding: .5em 1em;
			border: 1px solid #999;
			background: #eee;
			min-width: 600px;
		}

		@media (min-width: 940px) {
			body {
				margin: 20px auto;
				max-width:860px;
			}
		}

		.utility {
			position: absolute;
			right: 4em;
			top: 145px;
			font-size: 0.85em;
		}
		.utility li {
			display: inline;
		}




		ul {
			list-style: none;
			margin: 0;
			padding: 0;
		}
		#head ul li, dl ul li, #foot li {
			list-style: none;
			display: inline;
			margin: 0;
			padding: 0 0.2em;
		}
		ul.aliases, ul.projects, ul.tools {
			list-style: none;
			line-height: 25px;
		}
		ul.aliases a, ul.projects a{
			padding-left: 24px;
			padding-bottom: 5px;
			padding-top: 5px;
			background: url(/tools/subdir.png) 0 100% no-repeat;
		}

		ul.projects a.local {
			background-image:url(/tools/home.png);
		}
		ul.tools a, ul.tools li.admin a {
			padding-right: 22px;
			background: url(/?img=pngWrench) right center no-repeat;
			font-weight: normal;
		}
		ul.aliases a, input.button, ul.tools li.admin a {
			background: url(/?img=pngFolderGo) 0 100% no-repeat;
		}
		ul.tools li.adminer a {
			background-image: url(/tools/adminer24.png);
			padding-right: 30px;
			line-height: 30px;
		}
		ul.tools a, ul.tools li.admin a {
			background-position: right center;
		}

		input.button {
			min-width:120px;
			cursor: pointer;
		}
		dl {
			margin: 0;
			padding: 0;
		}
		dt {
			font-weight: bold;
			text-align: right;

		}
		dd {
			overflow: auto;
		}


		a {
			color: #024378;
			font-weight: bold;
			text-decoration: none;
		}
		a:hover {
			color: #04569A;
			text-decoration: underline;
		}
		a img,:link img,:visited img {
			border: none;
		}

		#foot{
			margin-top: 1.8em;
			border-top: 1px solid #999;
			padding-top: 1em;
			font-size: 0.85em;
		}
		#phpinfo, #errorLog {

			margin-top: 1em;
			margin-bottom: 1em;
			border-bottom: 1px solid #999;
			padding-bottom: 1.8em;
		}
		#phpinfo .center {
			font-size: .8em;
		}

		#phpinfo td, #phpinfo th, #phpinfo h1, #phpinfo h2 {font-family: sans-serif;}
		#phpinfo pre {margin: 0px; font-family: monospace;}
		#phpinfo a:link {color: #000099; text-decoration: none; background-color: #ffffff;}
		#phpinfo a:hover {text-decoration: underline;}
		#phpinfo table {border-collapse: collapse; min-width:800px;}
		#phpinfo .center {text-align: center;}
		#phpinfo .center table, #phpinfo .center hr { margin-left: auto; margin-right: auto; text-align: left;}
		#phpinfo .center th { text-align: center !important; }
		#phpinfo td, #phpinfo th { border: 1px solid #000000; vertical-align: baseline; padding: 2px; }
		#phpinfo h1 {font-size: 18px;}
		#phpinfo h2 {font-size: 16px;}
		#phpinfo .p {text-align: left;}
		#phpinfo .e {background-color: #ccccff; font-weight: bold; color: #000000; text-align:right; width: 25%; white-space:nowrap;}
		#phpinfo .h {background-color: #9999cc; font-weight: bold; color: #000000;}
		#phpinfo .v {background-color: #FFF; color: #000000; max-width:300px; white-space:normal; word-wrap: break-word; }
		#phpinfo .vr {background-color: #cccccc; text-align: right; color: #000000;}
		#phpinfo img {float: right; border: 0px;}
		#phpinfo hr {width: 800px; background-color: #cccccc; border: 0px; height: 1px; color: #000000;}


		#foot th {
			text-align: center;
		}
		#foot a {
			font-weight: normal;
		}
		#foot th img {
			vertical-align:middle;
		}
		#foot td {
			white-space:nowrap;
			text-align: center;
			vertical-align: top;
		}

		#menu ul.tabs {
			list-style: none;
			margin: 0;
			padding: 0;
			clear: both;
			height: 22px;
			margin-top: 14px;
		}

		#menu ul.tabs li {
			float: left;
			position: relative;
			display: inline-block;
			margin-left: 4px;
			text-align: center;
			vertical-align: middle;
		}
		#menu ul.tabs li h2 {
			margin-right: 20px;
			margin-left: 20px;
		}

		#menu ul.tabs li a {
			padding: 10px;
			background-color: #fff;
			border: 1px solid #CCC;
			font-size: 14px;
		}

		#menu ul.tabs li a:hover {
			color: #666;
		}

		#menu ul.tabs li a.active {
			border-bottom: none;
			background-color: #EEE;
		}

		#menu h2 {
			margin:0;
			font-size: 1.2em;
		}

		.evaluator
		{
			/*float: left;
			width: 560px;*/
			border: 1px solid #ccc;
			padding: 10px;
			min-width: 600px;
			margin: 5px 0;
			font-size: .9em;
		}

		.evaluator .box
		{
			border: 1px solid #ccc;
			min-width: 550px;
			width:99%;
			padding: 3px;
			margin-bottom: 5px;
			/*overflow: auto;*/
		}

		.evaluator textarea {
			resize: vertical;
		}

		.parameters
		{
			float: left;
			width: 250px;
			border: 1px solid #ccc;
			padding: 10px;
		}

		.parameters p
		{
			padding: 1px;
		}

		.help
		{
			width: 410px;
			float: right;
			border: 1px solid #ccc;
			padding: 10px;
			padding-top:0;
			padding-right:0;
		}

		.help div
		{
			float: left;
			width: 20px;
		}


		.help div.description {
			float: left;
			width: 220px;
		}
		#regWrap p, .evaluator.reg p {
			margin: 1px;
			padding: 0;
		}

		.evaluator.reg input, .evaluator.reg textarea {
			border: 1px solid #134134;
			margin: 5px;
			width: 100%;
			padding: 1px;
		}
		input,textarea,select {
			padding: 3px;
			border: 1px solid #ccc;
		}
		input[type=text], select {
			font-size: 14px;
			background-color: #FFF;
		}
		input[type=submit]{
			font-weight: bold;
			background-color: #E6F4F7;

		}
		input[readonly] {

			background-color: #EEE;
		}

		.disabled {
			background-color: #dcdcdc;
		}

		input[disabled] {
			background-color: #dcdcdc;
		}
		.matches {	color: red; background-color:#FFF; }
		.matches0 {	color: #32CD32 	; }
		.matches1 {	color: #DA70D6; }
		.matches2 {	color: #FF8C00; }
		.matches3 {	color: #00FFFF; }
		.matches4 {	color: #32CD32; }
		.matches5 {	color: #8B0000; }
		.matches6 {	color: #2E8B57; }
		.matches7 {	color: #4B0082; }
		.matches8 {	color: #FF6347; }
		.matches9 {	color: #483D8B; }
		.matches10 {	color: #BDB76B; }

		#result {
			overflow: auto;
		}

		#code {
			color:darkblue;
		}



		.help dl { font-size:small; }
		.help dt { float: left; clear: left; text-align: right; font-weight: bold; color: green; }
		.help dd { margin: 0 0 0 40px;  }

		#myProjects {
			margin-top: 15px;
			font-size: .9em;
		}

		.mainTable {
			width: 100%;
			font-size: .9em;
		}

		.mainTable tbody tr:nth-child(odd) {
			background-color: #EFE;
		}
		.mainTable tbody tr:nth-child(1) {
			background-color: #DDD;
		}

		#regWrap {
			font-size: .87em;
		}

		.clear {
			clear:both;
		}

		.bad {
			color:red;
		}
		.neutral {
			color:blue;
		}
		.good {
			color:green;
		}
		</style>
	</head>
	<body>

		<div id="main">
			<div id="menu">
				<ul class="tabs" id="menu_tabs">
					<li><h2 title="Redis Cache Manager">Redis</h2></li>
					<li><?php $active=( is_null($db_num) ? ' class="active"' : '' ); echo "<a$active href='" . $script_name . "?a=i'>[ Info ]</a>"; ?></li>
					<?php display_databases( $redis, $db_num ); ?>
				</ul>
			</div>

			<div class="evaluator redis">


				<?php



				/***************************************************************************
				*
				* parse parameters for action, sort and pattern
				*
				*/

				$action  = "b";
				$sort    = "key";
				$pattern = "*";

				if (isset($_REQUEST['a']) && !empty($_REQUEST['a'])) {
					$action = $_REQUEST['a'];
				}

				if (isset($_REQUEST["s"]) && !empty($_REQUEST["s"])) {
					if (!in_array($_REQUEST["s"], ["ttl", "key", "sz", "1", "no"])) {
						die();
					}

					$sort = $_REQUEST["s"];
				}

				if (isset($_REQUEST["p"]) && !empty($_REQUEST["p"])) {
					$pattern = $_REQUEST["p"];
				}



				/*********************************************
				*
				*    check if server is available
				*
				*/

				try {
					$res = $redis->ping();
				} catch (Exception $e) {
					echo "Error Connecting to Redis Server (Redis server is available only on 64bit systems)";
					die;
				}




				/***************************************************************************
				*
				* select database
				*
				*/

				if ( ! is_null( $db_num ) ) {
					$redis->select( $db_num );
				}










				/***************************************************************************
				*
				* handle actions
				*
				*/

				switch( $action ) {

					case "s": {   // show
						if (isset($_REQUEST["k"])) {
							$k = base64_decode($_REQUEST["k"]);
							display_key( $k );
							die;
						}

						break;
					}


					case "as": {   // add string
						if (isset( $_REQUEST['key']) && isset($_REQUEST['val'])) {
							$redis->set($_REQUEST['key'], $_REQUEST['val']);
						}

						break;
					}

					case "ah": {   // add hash
						if (isset( $_REQUEST['key']) && isset($_REQUEST['val']) && isset($_REQUEST['hash'])) {
							$redis->hset( $_REQUEST['hash'], $_REQUEST['key'], $_REQUEST['val'] );
						}

						break;
					}

					case "d": {   // delete
						if (isset($_REQUEST['sel']) && count($_REQUEST['sel']) > 0) {
							foreach ($_REQUEST['sel'] as $s) {
								$redis->del(base64_decode($s));
							}
						}

						break;
					}

					case "f": {   // flush DB
						$redis->flushdb();
						break;
					}

					case "p": {    // make persistent
						if (isset($_REQUEST['sel']) && count($_REQUEST['sel']) > 0) {
							foreach ($_REQUEST['sel'] as $s) {
								$redis->persist(base64_decode($s));
							}
						}

						break;
					}

					case "i": {   // display server info
						display_info($redis);
						die; // done here
					}

				}


				/***************************************************************************
				*
				*
				* done with actions - display the rest of the header section
				*
				*/





				echo '<br><center><form name="search" method="get" action="' . $script_name . '">';
				echo '<input type="hidden" name="db" value="' . intval( $db_num ) . '" />';
				echo 'Pattern <input type="text" size=30 name="p" value="' . $pattern . '" />';
				echo '<input type="submit" value="Search" /> ';

				echo 'sort by: ';
				echo '<input type="radio" name="s"  value="key"' . ( $sort == 'key' ? "checked" : "" ) .  ' />Key ';
				echo '<input type="radio" name="s" value="sz" '  . ( $sort == 'sz' ? "checked" : ""  ) .  ' />Size ';
				echo '<input type="radio" name="s" value="ttl" ' . ( $sort == 'ttl' ? "checked" : "" ) .  ' />TTL ';
				echo '<input type="radio" name="s"  value="no"'  . ( $sort == 'no' ? "checked" : ""  ) .  ' />No ';

				echo '</form></center>' . "<br/>\n";





				/***************************************************************************
				*
				*
				* done with the header section - display the keys
				*
				*/


				$count_all_keys_in_db = $redis->dbsize();

				$all_keys     = [];
				$matched_keys = $redis->keys( $pattern );

				foreach ($matched_keys as $k) {
					$sz = -1;

					$type = $redis->type( $k );
					$ttl  = $redis->ttl ( $k );
					$sz   = getSize($k, $type);

					if (!isset( $all_keys[$type])) {
						$all_keys[$type] = [];
					}

					array_push($all_keys[$type], ["key" => $k, "ttl" => $ttl, "sz" => $sz]);
				}

				// sort by type
				ksort($all_keys);

				util_html_form_start("form_select", $pattern, $sort, "post", false);

				echo "Showing " . count($matched_keys) . " of " . $count_all_keys_in_db . " keys";

				?>
				&nbsp;&nbsp;
				<select name="a">
					<option value="d">Delete selected</option>
					<!-- <option value="p">Persist selected</option> -->
					<option value="f">Flush DB</option>
				</select>
				<input type="submit" value="Execute" onClick="return confirmSubmit()" /><br/><br/>


				<table class="mainTable" border="0" cellpadding="2" cellspacing="1">
				<tr>
					<th align=center  width='40'>
						<input type="checkbox" name="check_all" value="Check All" onClick="javascript:selectToggle('form_select');">
					</th>
					<th align=left width='50'> Type </th>
					<th align=left > Key </th>
					<th align=center width='75'> Size </th>
					<th align=center width='75'> TTL </th>
				</tr>



				<?php

				foreach ($all_keys as $type => $keys) {


					$typeTitle = getTypeTitle($type);

					switch ($sort) {
						case "key" : uasort($keys, 'util_custom_cmp_key'); break;
						case "ttl" : uasort($keys, 'util_custom_cmp_ttl'); break;
						case "sz"  : uasort($keys, 'util_custom_cmp_sz');  break;
					}

					foreach ($keys as $k) {
						$ttl_txt = util_format_ttl($k['ttl']);
						$sz_txt  = util_format_size($type, $k['sz']);

						echo '<tr><td align=center><input type="checkbox" name="sel[]" value="' . htmlspecialchars(base64_encode($k['key'])) . '" />' . "</td>";
						echo "<td>$typeTitle</td><td><a href='$script_name?a=s&db=".intval($db_num)."&k=".htmlspecialchars(base64_encode($k['key'])) ."'>" . $k['key'] . "</a> </td>";
						echo "<td  align=center>" . $sz_txt . "</td>";
						echo "<td  align=center>" . $ttl_txt . "</td>";
						echo "</tr>\n";
					}

				}
				echo "</table>";
				echo "</form>";
				echo "<br/><br/>";


				util_html_form_start("as", '*', $sort);
				?>
					<input type="text" name="key" value="<key>"			onfocus="this.value==this.defaultValue?this.value='':null"/>
					<input type="text" name="val" value="<value>"		onfocus="this.value==this.defaultValue?this.value='':null"/>
					<input type="submit" value="Create String" /><br/>
				</form>

				<?php util_html_form_start("ah", '*', $sort); ?>
					<input type="text" name="hash" 	value="<key>" 		onfocus="this.value==this.defaultValue?this.value='':null"/>
					<input type="text" name="val" 	value="<field>"		onfocus="this.value==this.defaultValue?this.value='':null"/>
					<input type="text" name="key" 	value="<value>"		onfocus="this.value==this.defaultValue?this.value='':null"/>
					<input type="submit" value="Create Hash" /><br/>
				</form>

				<script>
					function selectToggle (n) {
						var fo = document.forms[n];
						var t = fo.elements['check_all'].checked;

						for (var i = 0; i < fo.length; i++)  {
							if (fo.elements[i].name == 'check_all' ) {
								continue;
							}

							if (t) {
								fo.elements[i].checked = "checked";
							} else {
								fo.elements[i].checked = "";
							}
						}
					}

					function confirmSubmit () {
						if (document.form_select.a.selectedIndex !== 2) {
							return true;
						}

						return confirm("Are you sure you wish to continue?");
					}
				</script>
			</div>
		</div>
	</body>
</html>







<?php


/***************************************************************************
*
* util functions
*
*
*/


function display_databases ($redis, $curr_db) {
	global $script_name, $sort, $pattern;

	$num = util_get_dbs($redis);

	for ($n = 0; $n < $num; $n++) {
		$active = !is_null($curr_db) && $n == $curr_db ? 'class="active"' : '';
		echo "<li><a {$active} href='" . $script_name . "?db=" . $n . "&sort=" . $sort . "&p=" . htmlspecialchars($pattern ?? '') . "'>#" . $n . "</a></li>";
	}
}



function display_info ($redis) {
	$ts_lastsave = $redis->lastsave();
	$secs = time() - $ts_lastsave;

	echo "<center>Last save " . $secs . " seconds ago. <br/><br/>";

	$info = $redis->info();

	echo '<table class="mainTable" border="0" cellpadding="3" cellspacing="1" width="50%">';

	foreach ($info as $k => $v) {
		if ($k == 'allocation_stats') {
			$v = str_replace(",", "<br/>", $v);
		}

		#if( substr( $k, 0, 2 ) == "db" ) {
		#	//$v = "Keys: " . $v['keys'] . "<br/>Expires: " . $v['expires'];
		#}

		echo '<tr><td>' .  $k . "</td>";
		echo '<td>' .  $v . "</td></tr>";
	}

	echo "</table></body></html>";
}


function getTypeTitle ($type) {
	switch ($type) {
		case Redis::REDIS_STRING: return 'String';
		case Redis::REDIS_HASH:   return 'Hash';
		case Redis::REDIS_LIST:   return 'List';
		case Redis::REDIS_SET:    return 'Set';
		case Redis::REDIS_ZSET:   return 'Sorted Set';
		default: return ' UNKNOWN ';
	}
}

function getSize ($k, $type) {
	global $redis;

	switch ($type) {
		case Redis::REDIS_STRING: return $redis->strLen($k);
		case Redis::REDIS_HASH:   return $redis->hLen($k);
		case Redis::REDIS_LIST:   return $redis->lLen($k);
		case Redis::REDIS_SET:    return $redis->sCard($k);
		case Redis::REDIS_ZSET:   return $redis->zCard($k);
		default: return null;
	}
}


function getKey ($k, $type) {
	global $redis;

	switch ($type) {
		case Redis::REDIS_STRING: return $redis->get($k);
		case Redis::REDIS_HASH:   return $redis->hGetAll($k);
		case Redis::REDIS_LIST:   return $redis->lRange($k, 0, -1 );
		case Redis::REDIS_SET:    return $redis->sMembers($k);
		case Redis::REDIS_ZSET:   return $redis->zRange($k, 0, -1, ['withscores' => true]);
		default: return null;
	}
}


function display_key ($k) {

	global $redis;

	$type      = $redis->type($k);
	$typeTitle = getTypeTitle($type);
	$retval    = false;

	echo "<pre>";

	$retval = getKey($k, $type);

	echo "Key:  " . $k 		. "\n";
	echo "Type: " . $typeTitle 	. "\n";

	if (isset($_REQUEST["u"])) {
		$retval = unserialize($retval);
		echo "Unserialized\n";

	} else {
		if (isset($_SERVER['REQUEST_URI'])) {
			$u = $_SERVER['REQUEST_URI'] . "&u=1";
			echo "<a href='". $u . "'>Unserialize</a>\n";
		}
	}

	if (isset($_REQUEST["s"])) {
		asort($retval);
		echo "Sorted by values\n";

	} else {
		if (isset($_SERVER['REQUEST_URI'])) {
			$u = $_SERVER['REQUEST_URI'] . "&s=1";
			echo "<a href='". $u . "'>Sort array by values</a>\n";
		}
	}

	echo "\n";
	echo htmlentities(print_r($retval, true));
}


function util_get_dbs ($redis) {
	return 11;
}


function util_format_ttl ($ttl) {
	if ($ttl === -1) {
		return $ttl;
	}

	$m = (int) ($ttl / 60);

	if ($m > 120) {
		$m = (int) ($m / 60);
		$s = "" . $m . "h";
	} else {
		$s = "" . $m . "min";
	}

	$s = $ttl . " (" . $s . ")";

	return $s;
}


function util_format_size ($type, $sz) {
	$s = $sz;

	if ($type === "string" && (int) $sz > 1100) {
		$s = (int)($sz / 1024) . "kb";
	}

	return $s;
}


function util_custom_cmp_key ($a, $b) {
	return strcmp($a['key'], $b['key']);
}


function util_custom_cmp_ttl ($a, $b) {
	if ($a['ttl'] === $b['ttl']) {
		return util_custom_cmp_key($a, $b);
	}

	return $a['ttl'] - $b['ttl'];
}


function util_custom_cmp_sz ($a, $b) {
	if ($a['sz'] === $b['sz']) {
		return util_custom_cmp_key($a, $b);
	}

	return $a['sz'] - $b['sz'];
}


function util_html_form_start ($action, $pattern, $sort, $type = "post", $put_action = true) {
	global $script_name, $db_num;

	echo '<form name="' . $action . '" method="' . $type . '" action="' . $script_name . '">';
	if ($put_action) echo '<input type="hidden" name="a" value="' . $action . '" />';
	echo '<input type="hidden" name="p" value="' . htmlspecialchars($pattern) . '" />';
	echo '<input type="hidden" name="s" value="' . htmlspecialchars($sort) . '" />';
	echo '<input type="hidden" name="db" value="' . intval($db_num) . '" />';
}
