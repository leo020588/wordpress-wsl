<?php

define( 'DB_NAME', '~DB_NAME~' );
define( 'DB_USER', '~DB_USER~' );
define( 'DB_PASSWORD', '~DB_PASSWORD~' );
define( 'DB_HOST', '~DB_HOST~' );
define( 'DB_CHARSET', '~DB_CHARSET~' );
define( 'DB_COLLATE', '~DB_COLLATE~' );

define( 'REDIS_HOST', '127.0.0.1' );
define( 'REDIS_PORT', 6379 );
define( 'REDIS_AUTH', null );
define( 'REDIS_DATABASE', 0 );

define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', false );
define( 'WP_DEBUG_LOG', '~WP_DEBUG_LOG~' );
define( 'WP_DEBUG_ERROR_LEVEL', 32767 );

define( 'AUTH_KEY', '~AUTH_KEY~' );
define( 'AUTH_SALT', '~AUTH_SALT~' );
define( 'SECURE_AUTH_KEY', '~SECURE_AUTH_KEY~' );
define( 'SECURE_AUTH_SALT', '~SECURE_AUTH_SALT~' );
define( 'LOGGED_IN_KEY', '~LOGGED_IN_KEY~' );
define( 'LOGGED_IN_SALT', '~LOGGED_IN_SALT~' );
define( 'NONCE_KEY', '~NONCE_KEY~' );
define( 'NONCE_SALT', '~NONCE_SALT~' );
define( 'WP_CACHE_KEY_SALT', '~WP_CACHE_KEY_SALT~' );

define( 'WP_MEMORY_LIMIT', '64M' );  // front
define( 'WP_MAX_MEMORY_LIMIT', '128M' ); // admin

define( 'WP_ENVIRONMENT_TYPE', 'production' );
define( 'WP_DEFAULT_THEME', '~THEME_SLUG~' );
define( 'DISABLE_WP_CRON', true );

define( 'WP_POST_REVISIONS', -1 );
define( 'AUTOSAVE_INTERVAL', 60 );

define( 'DISALLOW_FILE_EDIT', true );
define( 'DISALLOW_FILE_MODS', true );

define( 'WP_AUTO_UPDATE_CORE', false );
define( 'AUTOMATIC_UPDATER_DISABLED', true );
define( 'CORE_UPGRADE_SKIP_NEW_BUNDLE', true );

define( 'CONCATENATE_SCRIPTS', false );
define( 'COMPRESS_SCRIPTS', false );
define( 'COMPRESS_CSS', false );

$table_prefix = '~DB_PREFIX~';

if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

require_once ABSPATH . 'wp-settings.php';
